import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListallinsurancesComponent } from './listallinsurances.component';

describe('ListallinsurancesComponent', () => {
  let component: ListallinsurancesComponent;
  let fixture: ComponentFixture<ListallinsurancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListallinsurancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListallinsurancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
