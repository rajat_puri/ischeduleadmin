


import { Component, OnInit } from "@angular/core";
import { DoctorService } from "../services/doctor.service";
import { Router } from "@angular/router";


// import Swal from "sweetalert2";
declare var swal: any;
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import {
  ToasterContainerComponent,
  ToasterService,
  ToasterConfig
} from "angular2-toaster";


declare var $;

@Component({
  selector: 'app-listalldoctors',
  templateUrl: './listalldoctors.component.html',
  styleUrls: ['./listalldoctors.component.css'],
  providers: [DoctorService]
})
export class ListalldoctorsComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean = false;
  allAppointments: any = [];
  appStatus: String = ""
  alldoctors: any = []
  constructor(
    private doctorService: DoctorService,
    private fb: FormBuilder,
    private ts: ToasterService,
    private router: Router
  ) { }
  ngOnInit() {
    this.loading = true;
    this.getAllDoctors();

  }


  approve(appId) {


    swal({
      title: "Are you sure want to approve this appointment ",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "YES  !"
    }).then(result => {
      console.log(result);
      if (result) {

        this.doctorService.approveAppointment(appId).subscribe(res => {
          this.loading = false;
          if (res.IsSuccess) {
            this.ts.pop("success", "", "Appointment  successfully approved")
            //    this.getAllAppointments()
          } else {
            this.ts.pop("error", "", res.Message)

          }
        });

      }
    });



  }

  viewProfile(id) {
    this.router.navigate(["/doctor/patientProfile/" + id]);

  }


  selectChange(appId) {
    swal({
      title: "Are you sure want to change the status of this appointment ",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "YES  !"
    }).then(result => {
      console.log(result);
      if (result) {
        let datatosend = {
          status: this.appStatus,
          appId: appId
        }
        this.doctorService.changeAppointmentStatus(datatosend).subscribe(res => {
          this.loading = false;
          if (res.IsSuccess) {
            this.ts.pop("success", "", "Appointment  successfully disapproved")
            //  this.getAllAppointments()
          } else {
            this.ts.pop("error", "", res.Message)

          }
        });

      }
    });


  }
  getAllDoctors() {
    this.doctorService.getAllDoctors().subscribe(res => {
      this.loading = false;
      if (res.IsSuccess) {
        this.alldoctors = res.Data;
        $('select').selectpicker('refresh');
      } else {
        this.ts.pop("error", "", res.Message)
      }
    });
  }


}
