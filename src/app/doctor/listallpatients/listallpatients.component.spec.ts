import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListallpatientsComponent } from './listallpatients.component';

describe('ListallpatientsComponent', () => {
  let component: ListallpatientsComponent;
  let fixture: ComponentFixture<ListallpatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListallpatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListallpatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
